if (!window.mdhtr) {
  window.mdhtr = {};
}
window.mdhtr.fileProcessing = window.mdhtr.fileProcessing || function() {
  function openFile() {
    hideDownloadButton();
    const fileInput = document.getElementById('fileInput');
    fileInput.value = null;
    fileInput.click();
  }

  function readFile(e) {
    const file = e.target.files[0];
    if (!file || file.type !== 'text/plain') return;
    const reader = new FileReader();
    reader.onload = function(e) {
      const fileText = e.target.result;
      const result = window.mdhtr.cibExportParser.parseFile(fileText);
      displayDownloadButton(result, file.name + '.csv');
    };
    reader.readAsText(file, 'iso-8859-1');
  }

  function hideDownloadButton() {
    const downloadButton = document.getElementById('downloadFileButton');
    downloadButton.removeAttribute('data-download-url');
    downloadButton.classList.add('is-hidden');
  }

  function displayDownloadButton(result, filename) {

    const isMac = !!navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i);

    if (!isMac) {
      const BOM = '\ufeff';
      result = BOM + result;
    }

    const blob = new Blob([result], { encoding: "UTF-8", type: 'text/csv;charset=utf-8;' });
    const url = window.URL.createObjectURL(blob);

    const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

    const downloadButton = document.getElementById('downloadFileButton');
    if (!window.navigator.msSaveOrOpenBlob) {
      downloadButton.href = url;
      downloadButton.download = filename;
    }
    downloadButton.onclick = function() {
      if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, fileName);
      } else if (isSafari) {
        window.open('data:attachment/csv;charset=utf-8,' + encodeURI(result));
      }
    }

    downloadButton.classList.remove('is-hidden');
  }

  return {
    openFile,
    readFile
  };
}();