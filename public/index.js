if (!window.mdhtr) {
  window.mdhtr = {};
}
document.getElementById('openFileButton')
  .addEventListener('click', () => window.mdhtr.fileProcessing.openFile());
document.getElementById('fileInput')
  .addEventListener('change', (e) => window.mdhtr.fileProcessing.readFile(e));
