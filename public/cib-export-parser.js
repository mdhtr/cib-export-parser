if (!window.mdhtr) {
    window.mdhtr = {};
}
window.mdhtr.cibExportParser = window.mdhtr.cibExportParser || function() {
    function parseFile(fileText) {
        const rawdata = fileText.split(/\r?\n/);
        let result = "";
        const entries = [];
        const transactions = [];

        const capture_pattern = /^\d{4}\.\d{2}\.\d{2}\./;
        let candidate = [];
        for (let i = 0; i < rawdata.length; i++) {
            const row = rawdata[i].trim();
            const isCandidate = capture_pattern.test(row)
            const isFirstRow = candidate.length === 0 && isCandidate;
            const isSecondRow = candidate.length === 1 && isCandidate;
            const isEmptyRow = row === "";
            const isAdditionalRow = candidate.length >= 2 && !isEmptyRow;
            if (isFirstRow || isSecondRow || isAdditionalRow) {
                candidate.push(row);
            }
            else if (candidate.length !== 0) {
                entries.push(candidate.slice());
                candidate = [];
            }
    }

        function clean_up_text(transaction) {
            const type =  transaction['type'];
            const text =  transaction['text'];
            if (['BANKKÁRTYA TERHELÉS', 'BANKKÁRTYA JÓVÁÍRÁS', 'KÁRTYA KPFELVÉTEL'].includes(type)) {
                transaction['text'] = text.replace(/^(.+?(?=;);){2}/, '').trim().substring(0, 18);
            }
            else if (['TERHELÉS', 'JÓVÁIRÁS', 'Díjnet számla kiegyenlítés'].includes(type)) {
                transaction['text'] = text.replace(/^.+?(?=;);/, '').trim();
            }
            else if (['Kimenõ eseti utalás', 'Bankon belüli eseti jóváírás', 'Kimenõ rendsz. utalás'].includes(type)) {
                transaction['text'] = text.replace(/^(.+?(?= )){2} /, '').trim();
            }
            else if (['Bejövõ giro jóváírás', 'Csoportos átutalás jóváírás'].includes(type)) {
                transaction['text'] = text.replace(/^(.+?(?= )){8} /, '').trim();
            }
        }

        for (let i = 0; i < entries.length; i++) {
            const entry = entries[i];
            const transaction = {};
            for (let j = 0; j < entry.length; j++) {
                let row = entry[j];
                if (row === entry[0]) {
                    const parts = row.replaceAll(/  +/g, '\t').split('\t');
                    for (let k = 0; k < parts.length; k++) {
                        const part = parts[k];
                        parts[k] = part.trim();
                    }
                    const n = parts.length;
                    const text = parts.slice(1, n-2).join(' ').split(';')
                    transaction['date'] = parts[0];
                    transaction['type'] = text[0];
                    transaction['text'] = text.slice(1).join(';').trim();
                    transaction['value'] = parts[n-2];
                    transaction['rollingsum'] = parts[n-1];
                }
                else {
                    const isCandidate = capture_pattern.test(row);
                    if (isCandidate) {
                        row = row.substring(11).trim();
                    }
                    transaction['text'] = [transaction['text'], ' ', row].join('');
                }
            }
            clean_up_text(transaction);
            transactions.push(transaction);
        }

        function getLineEnding() {
            return navigator.appVersion.indexOf('Win')!==-1 ? '\r\n': '\n';
        }
        const lineEnding = getLineEnding();

        result = result.concat(['KÖNYVELÉSI/ÉRTÉKNAP', 'TÍPUS', 'TRANZAKCIÓ', 'TERHELÉSEK(-)/JÓVÁÍRÁSOK', 'EGYENLEG', lineEnding].join('\t'));
        for (let i = 0; i < transactions.length; i++) {
            const transaction = transactions[i];
            result = result.concat([transaction['date'], transaction['type'], transaction['text'], transaction['value'], transaction['rollingsum'], lineEnding].join('\t'));
        }
        return result;
    }

    return {
        parseFile
    };
}();