# cib-export-parser
The goal of this project is to provide a way to convert the useless text export file 
of the Hungarian bank CIB (the export from the Java desktop app) to a more usable format.

Try it out on the [GitLab Page](https://mdhtr.gitlab.io/cib-export-parser/)